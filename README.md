**Phone:** (330) 569-4896  
**Email:** brandon.chartier@gmail.com  
**Location:** Cleveland, OH

## About
Highly skilled at writing applications in several language; strengths in JavaScript (Node.js), HTML(5),
CSS(3), with experience writing Android and iOS applications. Formal training in systems and network
administration, with production experience managing Linux servers.

## Skills

**Advanced**  
JavaScript, Node.js, HTML, CSS, Git, React, Backbone, Bootstrap

**Proficient**  
Python, Ruby

**Some Experience**  
C#, Java

## Work

**Dealer Tire**  
_Consultant_  
2016  
Migrating a large ColdFusion platform to Node.js, adding new key features, focusing on performance and code reusability.  
Clients: Dealer Tire

**Futuri Media**  
_Web Application Developer_  
2015  
Working in an agile development environment to create Android and iOS applications using PhoneGap
(Cordova), Backbone and React.  
Clients: Dayton.com, Cox Media Group

**Cleveland Clinic Foundation**  
_Web Development Manager_  
2013-2015 (Until present, as a consultant)  
Managing the development team on various sites and applications; Building applications in Node.js,
managing Linux servers and Elasticsearch databases; Maintaining DotNetNuke and Wordpress content
management systems.  
Clients: Cleveland Clinic Laboratories

**Rosetta**  
_Senior Interactive Developer_  
2011-2013  
Developing interactive applications in JavaScript, CSS, HTML, and Objective-C. Managing junior-
level employees and resources, preparing and presenting training courses and other classes.  
Clients: Samsung, Canada Post, Allergan, Novartis

**Metrics Marketing, LLC**  
_Web Developer_  
2010-2011  
Creating responsive websites in JS, CSS and HTML; Using DotNetNuke, Wordpress, and Drupal.  
Clients: Akron Brass, Hyatt Legal Plans, Visit Las Vegas

**DigiKnow, Inc.**  
_Content Developer and Network Administrator_  
2006-2010  
Producing and maintaining websites for mostly regional clients. Managing Windows and Unix servers;
DNS, Active Directory, Network security, Phones, Exchange, Backups, First-level support.  
Clients: Goodyear, Lincoln Electric, Nestle, Cleveland Browns
